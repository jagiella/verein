#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 29 22:00:43 2020

@author: nick
"""

import sys
import numpy as np
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtSql import QSqlDatabase, QSqlTableModel

from verein import Database
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

class SollTableModel(QtCore.QAbstractTableModel):
    def __init__(self, db, start='2020-01-01', end='2020-07-31'):
        super(SollTableModel, self).__init__()
        self.db = db
        
        self.d0 = datetime.strptime(start, "%Y-%m-%d")
        self.d1 = datetime.strptime(end,   "%Y-%m-%d")
        self.n_months = relativedelta(self.d1, self.d0).months + 1
        
        query = self.db.exec('SELECT * FROM kinder')
        self._data = []
        while query.next():
            self._data.append([query.value(0),query.value(1),query.value(2)])
        self._data = np.asarray(self._data)
            # country = query.value(0)
                
        # self._data = self.db.getTable('kinder')
        # self._tarifs = np.zeros((self._data.shape[0], self.n_months))
        # for row_id, kind_id in enumerate(self._data[:,0]):
        #     print('kind_id', kind_id)
        #     self.db.cur.execute('''
        #             SELECT * 
        #             FROM kind_tarifgruppe
        #             WHERE kind_ID="%s" ''' % (kind_id))
        #     for row in self.db.cur.fetchall():
                
        #         print( row)
        #         d0 = datetime.strptime( row[2], "%Y-%m-%d")
        #         if( d0 < self.d0):
        #             d0 = self.d0
        #         if( row[3] is not None):
        #             d1 = datetime.strptime( row[3], "%Y-%m-%d")
        #             if( d1 > self.d1):
        #                 d1 = self.d1
        #         else:
        #             d1 = self.d1
                
        #         d = d0
        #         i = 0
        #         while( d < d1):
        #             # print(d.strftime("%Y-%m"))
        #             self._tarifs[row_id, i] = row[1]
        #             d += relativedelta(months=1)
        #             i += 1
            

    def data(self, index, role):
        if role == Qt.DisplayRole:
            # See below for the nested-list data structure.
            # .row() indexes into the outer list,
            # .column() indexes into the sub-list
            if( index.column() < 3):
                return str(self._data[index.row()][index.column()])
            else:
                return str(self._tarifs[index.row()][index.column()-3])
                

    def setData(self, index, value, role):
        if role == Qt.EditRole:
            self._data[index.row()][index.column()] = value
            self.db.insertRow( 'kinder', ['ID', 'Name', 'Vorname'], self._data[index.row()])
            return True
        
    def flags(self, index):
        return Qt.ItemIsEnabled | Qt.ItemIsSelectable | Qt.ItemIsEditable

    def rowCount(self, index):
        # The length of the outer list.
        return self._data.shape[0]

    def columnCount(self, index):
        # The following takes the first sub-list, and returns
        # the length (only works if all rows are an equal length)
        return self._data.shape[1] + self.n_months


class TableModel(QtCore.QAbstractTableModel):
    def __init__(self, db):
        super(TableModel, self).__init__()
        self.db = db
        data = self.db.getTable('kinder')
        self._data = data

    def data(self, index, role):
        if role == Qt.DisplayRole:
            # See below for the nested-list data structure.
            # .row() indexes into the outer list,
            # .column() indexes into the sub-list
            return str(self._data[index.row()][index.column()])

    def setData(self, index, value, role):
        if role == Qt.EditRole:
            self._data[index.row()][index.column()] = value
            self.db.insertRow( 'kinder', ['ID', 'Name', 'Vorname'], self._data[index.row()])
            return True
        
    def flags(self, index):
        return Qt.ItemIsEnabled | Qt.ItemIsSelectable | Qt.ItemIsEditable

    def rowCount(self, index):
        # The length of the outer list.
        return self._data.shape[0]#len(self._data)

    def columnCount(self, index):
        # The following takes the first sub-list, and returns
        # the length (only works if all rows are an equal length)
        return self._data.shape[1]#len(self._data[0])


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()

        self.table = QtWidgets.QTableView()

        # self.db = Database( 'doppiomondo.db')
        self.db = QSqlDatabase.addDatabase('QSQLITE')
        self.db.setDatabaseName('doppiomondo.db')
        

        # data = [
        #   [4, 9, 2],
        #   [1, 0, 0],
        #   [3, 5, 0],
        #   [3, 3, 2],
        #   [7, 8, 9],
        # ]

        # self.model = SollTableModel( self.db)
        self.model = QSqlTableModel()
        self.model.setTable('kinder') 
        self.model.setEditStrategy(QSqlTableModel.OnFieldChange)
        self.model.select()
        self.table.setModel(self.model)

        self.setCentralWidget(self.table)


        self.sollTable = QtWidgets.QTableView()
        self.sollTable.setModel( SollTableModel( self.db))
        
        
        dw = QtWidgets.QDockWidget()
        dw.setWidget( self.sollTable)
        self.addDockWidget( Qt.RightDockWidgetArea, dw)


if( __name__=='__main__'):
    # start='2020-01-01'
    # end='2020-07-31'
    
    # d0, d1 = datetime.strptime(start, "%Y-%m-%d"), datetime.strptime(end, "%Y-%m-%d")
    
    # d = d0
    # while( d < d1):
    #     print(d.strftime("%Y-%m"))
    #     d += relativedelta(months=1)
        
    # n_months = relativedelta(d1,d0).months + 1
    # print( n_months)
    
    
    # dt = datetime.strptime(end, "%Y-%m-%d") - datetime.strptime(start, "%Y-%m-%d")
    app=QtWidgets.QApplication(sys.argv)
    window=MainWindow()
    window.show()
    app.exec_()