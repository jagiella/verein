#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 28 21:57:21 2020

@author: nick
"""

import sqlite3
import csv
import os
import numpy as np



class Database:
    def __init__(self, filename):
        if( os.path.exists( filename)):            
            self.con = sqlite3.connect( filename)
            self.cur = self.con.cursor()
        else:            
            self.con = sqlite3.connect( filename)
            self.cur = self.con.cursor()

            # Create table
            self.cur.execute('''CREATE TABLE kinder (
                ID INTEGER NOT NULL UNIQUE,
                Name TEXT NOT NULL,
                Vorname TEXT NOT NULL,
                PRIMARY KEY(ID)
            );''')
            
            self.cur.execute('''CREATE TABLE eltern (
                ID INTEGER UNIQUE,
                Name TEXT NOT NULL,
                Vorname TEXT NOT NULL,
                kind_ID INTEGER NOT NULL,
                PRIMARY KEY(ID)
            );''')
            
            self.cur.execute('''CREATE TABLE tarifgruppen (
                ID INTEGER UNIQUE,
                Name TEXT NOT NULL,
                Betreuungsentgelt REAL NOT NULL,
                Mitgliedsbeitrag REAL NOT NULL,
                Essen REAL NOT NULL,
                Windeln REAL NOT NULL,
                PRIMARY KEY(ID)
            );''')
            
            self.cur.execute('''CREATE TABLE "kind_tarifgruppe" (
            	"kind_ID"	INTEGER,
            	"tarif_ID"	INTEGER,
            	# "monat"	INTEGER,
            	# "jahr"	INTEGER
                "start"     DATE,
                "ende"      DATE
            );''')
            
    def __del__(self):
        self.con.close()
            
    def importTableFromFile(self, table, filename, columns):
        with open(filename,'r') as fin: # `with` statement available in 2.5+
            # csv.DictReader uses first line in file for column headings by default
            dr = csv.DictReader(fin, delimiter=';') # comma is default delimiter
            to_db = [ [row[ column] for column in columns] for row in dr]
        # print( to_db)
        self.cur.executemany("INSERT INTO %s (%s) VALUES (%s);" % (table, str(columns).strip('[]'), ('?,'*len(columns))[:-1]), to_db)
        self.con.commit()
        
    def getTable(self, table):
        self.cur.execute("SELECT * FROM "+table)

        rows = self.cur.fetchall()

        for row in rows:
            print(row)
        return np.asarray( rows)
    
    def insertRow(self, table, column_list, value_list):
        s = 'UPDATE '+table+' SET ' 
        s+= ','.join( "%s = '%s'" % (column,value) for column, value in zip(column_list,value_list))
        s+= " WHERE %s = '%s'" % (column_list[0], value_list[0])
        print(s)
        self.cur.execute(s)
        
        # print('''REPLACE INTO %s (%s) VALUES (%s);''' % (table, ','.join(column_list), ','.join(value_list )))
        # self.cur.execute( '''REPLACE INTO %s (%s) VALUES (%s);''' % (table, ','.join(column_list), ','.join(value_list )))
        self.con.commit()
            
            
if( __name__ == '__main__'):
    db = Database( 'doppiomondo.db')
    
    # db.importTableFromFile( 'kinder', '/media/nick/OS/Dokumente und Einstellungen/Laptop/Documents/Doppiomondo/soll.csv', ['ID','Name','Vorname'])
    # db.importTableFromFile( 'tarifgruppen', '/media/nick/OS/Dokumente und Einstellungen/Laptop/Documents/Doppiomondo/tarifgruppen.csv', ['ID','Name','Betreuungsentgelt', 'Mitgliedsbeitrag', 'Essen', 'Windeln'])
    # db.importTableFromFile( 'kind_tarifgruppe', '/media/nick/OS/Dokumente und Einstellungen/Laptop/Documents/Doppiomondo/kind_tarifgruppe.csv', ['kind_ID','tarif_ID','monat','jahr'])

    # berechne Soll für 01/2020 bis 06/2020
    db.cur.execute('''SELECT k.Name, k.Vorname, kt.monat, kt.jahr, t.Betreuungsentgelt, t.Mitgliedsbeitrag, t.Essen, t.Windeln
                   FROM kinder k, kind_tarifgruppe kt, tarifgruppen t 
                   WHERE k.ID=kt.kind_ID AND t.ID=kt.tarif_ID''')
    rows = db.cur.fetchall()
    
    # bestimme tarifwechsel
    tarife = {}
    for row in rows:
        kind = (row[0], row[1])
        if( kind not in tarife):
            tarife[kind] = []
        tarife[kind].append( row[2:])
        
    